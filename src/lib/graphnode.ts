import { GraphEdge } from "./graphedge";

export interface GraphNode {
    // get index of this graphnode
    numericIndex(): number
    index(): any
    // edges to neighboring nodes
    edges(): GraphEdge[];
    // value contained in this node
    get?(): any
    heuristic?(): number
}