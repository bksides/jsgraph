import { clearLine } from "readline";
import { GraphEdge } from "./graphedge";
import { GraphNode } from "./graphnode";

export class MatrixGraphEdge2D implements GraphEdge {
    private _arr: any[][];
    private _from: [number, number];
    private _to: [number, number];

    constructor(arr: any[][], from: [number, number], to: [number, number]) {
        this._arr = arr;
        this._from = from;
        this._to = to;
    }

    from(): GraphNode {
        return new MatrixGraphNode2D(this._arr, this._from);
    }

    to(): GraphNode {
        return new MatrixGraphNode2D(this._arr, this._to);
    }
}

export class MatrixGraphNode2D implements GraphNode {
    private _index: [number, number];
    private _arr: any[][];

    constructor(arr: any[][], index: [number, number]) {
        this._arr = arr
        this._index = index
    }

    numericIndex(): number {
        return this._index[0]*this._arr[0].length + this._index[1];
    }

    index(): [number, number] {
        return this._index
    }

    edges(): MatrixGraphEdge2D[] {
        let ret:  MatrixGraphEdge2D[] = [];
        if(this._arr[this._index[0]][this._index[1]] == 0) {
            return ret;
        }
        if(this._index[0] > 0 && this._arr[this._index[0]-1][this._index[1]] != 0) {
            ret.push(new MatrixGraphEdge2D(
                this._arr, this._index, [this._index[0]-1, this._index[1]]
            ));
        }
        if(this._index[1] > 0 && this._arr[this._index[0]][this._index[1]-1] != 0) {
            ret.push(new MatrixGraphEdge2D(
                this._arr, this._index, [this._index[0], this._index[1]-1]
            ));
        }
        if(this._index[0] < this._arr.length-1 && this._arr[this._index[0]+1][this._index[1]] != 0) {
            ret.push(new MatrixGraphEdge2D(
                this._arr, this._index, [this._index[0]+1, this._index[1]]
            ));
        }
        if(this._index[1] < this._arr[this._index[0]].length-1 && this._arr[this._index[0]][this._index[1]+1] != 0) {
            ret.push(new MatrixGraphEdge2D(
                this._arr, this._index, [this._index[0], this._index[1]+1]
            ));
        }
        return ret;
    }

    manhattanDistance(cell: [number, number]): number {
        return Math.abs(this._index[0]-cell[0]) + Math.abs(this._index[1]-cell[1]);
    }

    heuristic(): number {
        let bestDist = Infinity;
        for(let [i, row] of this._arr.entries()) {
            for(let [j, cell] of row.entries()) {
                if(cell == 2 && this.manhattanDistance([i, j]) < bestDist) {
                    bestDist = this.manhattanDistance([i, j]);
                }
            }
        }
        return bestDist;
    }

    get(): any {
        return this._arr[this._index[0]][this._index[1]];
    }
}