import { GraphNode } from "./graphnode";
import { Heap } from "tsheap/heap";

export type GraphAlgorithmResultGenerator = Generator<[Map<number, string>, GraphNode | null]>;

export type GraphAlgorithm = (node: GraphNode, pred: (n: GraphNode) => boolean) => GraphAlgorithmResultGenerator;

export function* bfs(node: GraphNode, predicate: (n: GraphNode) => boolean): GraphAlgorithmResultGenerator {
    let queue: GraphNode[] = [node];
    let colorMap = new Map<number, string>();
    let parentMap = new Map<number, number | null>([[0, null]]);
    for(let curnode: GraphNode | undefined = queue.shift(); curnode != undefined; curnode = queue.shift()) {
        if(predicate(curnode)) {
            for(let [visited, color] of colorMap) {
                if(color == "red") {
                    colorMap.set(visited, "green");
                } else {
                    colorMap.delete(visited);
                }
            }
            for(let parent = parentMap.get(curnode.numericIndex()); parent != null; parent = parentMap.get(parent)) {
                colorMap.set(parent, "limegreen");
            }
            return [colorMap, curnode]
        } else {
            colorMap.set(curnode.numericIndex(), "red");
            for(let edge of curnode.edges()) {
                if(!colorMap.has(edge.to().numericIndex())) {
                    parentMap.set(edge.to().numericIndex(), curnode.numericIndex());
                    queue.push(edge.to());
                    colorMap.set(edge.to().numericIndex(), "darkred");
                }
            }
            yield [colorMap, curnode]
        }
    }
    return [colorMap, null];
}

export function* aStar(node: GraphNode, predicate: (n: GraphNode) => boolean): GraphAlgorithmResultGenerator {
    let distances = new Map<number, number>([[node.numericIndex(), 0]]);
    function distance(node: GraphNode): number {
        let index = node.numericIndex();
        return distances.has(index) ? distances.get(index) as number : Infinity;
    }

    let parents = new Map<number, number | undefined>([[node.numericIndex(), undefined]]);
    function* pathTo(node: GraphNode): Generator<number | undefined> {
        if(node == null) {
            return undefined;
        }
        let yieldIdx: number | undefined = node.numericIndex();
        while(yieldIdx != undefined) {
            yield yieldIdx;
            yieldIdx = parents.get(yieldIdx);
        }
    }

    function score(node: GraphNode) {
        return distance(node) + (node.heuristic ? node.heuristic() : 0);
    }

    let queue = new Heap<GraphNode>((a: GraphNode, b: GraphNode) => {
        return score(a) < score(b);
    }, [node], (a, b) => a.numericIndex() == b.numericIndex());

    let colorMap = new Map<number, string>();
    for(let nextNode of queue) {
        console.log("score for " + nextNode.index() + ": " + score(nextNode) + "\n");
        console.log(queue.data);
        colorMap.set(nextNode.numericIndex(), "red");
        if(predicate(nextNode)) {
            for(let [visited, color] of colorMap) {
                if(color == "red") {
                    colorMap.set(visited, "green");
                } else {
                    colorMap.delete(visited);
                }
            }
            for(let pathStep of pathTo(nextNode)) {
                colorMap.set(pathStep as number, "limegreen");
            }
            return [colorMap, nextNode];
        }
        for(let edge of nextNode.edges()) {
            if(colorMap.get(edge.to().numericIndex()) != "red") {
                colorMap.set(edge.to().numericIndex(), "darkred");
                if(distance(nextNode)+1 < distance(edge.to())) {
                    distances.set(edge.to().numericIndex(), distance(nextNode)+1);
                    parents.set(edge.to().numericIndex(), nextNode.numericIndex());
                    let indexInQueue = queue.find(edge.to());
                    if(indexInQueue != undefined) {
                        console.log("already in queue; sifted to " + queue.sift(indexInQueue));
                    } else {
                        queue.push(edge.to());
                    }
                }
            }
        }
        yield [colorMap, nextNode];
    }
    return [colorMap, null];
}

export function* dfs(node: GraphNode, predicate: (n: GraphNode) => boolean, colorMap: Map<number, string> = new Map()): GraphAlgorithmResultGenerator {
    colorMap.set(node.numericIndex(), "red");
    if(predicate(node)) {
        for(let [visited, _] of colorMap) {
            colorMap.set(visited, "green");
        }
        colorMap.set(node.numericIndex(), "limegreen");
        return [colorMap, node];
    } else {
        for(let edge of node.edges()) {
            if(!colorMap.has(edge.to().numericIndex())) {
                colorMap.set(edge.to().numericIndex(), "darkred");
            }
        }
        yield [colorMap, node];
    }
    for(let edge of node.edges()) {
        if(colorMap.get(edge.to().numericIndex()) != "red") {
            let result = yield* dfs(edge.to(), predicate, colorMap);
            if(result[1]) {
                colorMap.set(node.numericIndex(), "limegreen");
                return result;
            }
        }
    }
    return [colorMap, null];
}