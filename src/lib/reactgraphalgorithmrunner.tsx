import React, {useState, useEffect, ReactElement} from "react";
import { GraphNode } from "./graphnode";
import { dfs, bfs, aStar } from "./graphalgorithm";
import { MatrixGraphNode2D } from "./matrixgraph";

export class ReactGraphAlgorithmRunnerProps {
    arr: number[][];

    constructor(arr: number[][]) {
        this.arr = arr;
    }
}

function GraphCell(props: any) {
    return <div onClick={props.onClick} style={{backgroundColor: props.backgroundColor, borderRadius: "2px", width: 20, height: 20, display: "inline-block", margin: "2px"}}/>;
}

export function ReactGraphAlgorithmRunner(props: ReactGraphAlgorithmRunnerProps) {
    const algorithms = [dfs, bfs, aStar];
    const [algorithmSelection, setAlgorithmSelection] = useState(0);
    const [visited, setVisited] = useState<Map<number,string> | null>(null);
    const [arr, setArr] = useState<number[][]>(props.arr);
    const [current, setCurrent] = useState(new MatrixGraphNode2D(arr, [0, 0]));
    const [done, setDone] = useState(false);
    const [searchInterval, setSearchInterval] = useState<NodeJS.Timeout | null>(null);

    useEffect(() => {
        return () => {
            if(searchInterval != null) {
                clearInterval(searchInterval);
            }
        };
    }, []);
       
    let elems: ReactElement[] = [];
    //alert(visited);
    for(let [i, row] of arr.entries()) {
        let outRow: ReactElement[] = []
        for(let [j, cell] of row.entries()) {
            let index = i*row.length+j;
            outRow.push(<GraphCell backgroundColor={{0: "black", 2: "limegreen", 1: (current && index == current.numericIndex()) ? "yellow" : visited?.has(index) ? visited.get(index) : "darkgray"}[cell]
            } onClick={() => {
                arr[i][j] = (arr[i][j]+1)%3;
                setArr([...arr]);
            }}></GraphCell>);
        }
        elems.push(<div style={{margin: 0, padding: 0, maxHeight: "24px"}}>{outRow}</div>);
    }
    let algorithmOptions: ReactElement[] = []
    for(let [i, algopt] of algorithms.entries()) {
        algorithmOptions.push(<option value={i}>{algopt.name}</option>);
    }
    return <div style={{margin: "10px", padding: "15px", borderRadius: "15px", backgroundColor: "black", boxShadow: "0px 20px 10px -5px #555566"}}>
        {elems}
        <select value={algorithmSelection} onChange={(event: React.ChangeEvent<HTMLSelectElement>) => {
            setAlgorithmSelection(parseInt(event.target.value));
        }}>{algorithmOptions}</select>
        {(searchInterval == null || searchInterval == undefined ? <span style={{color: "white"}} onClick={() => {
                const resgen = algorithms[algorithmSelection](current, (n: GraphNode): boolean => {
                    return n.get ? n.get() == 2 : false;
                });
                let searchInterval = setInterval(() => {
                    let next_result = resgen.next();
                    if (next_result.done) {
                        clearInterval(searchInterval as NodeJS.Timeout);
                        if(next_result.value != undefined && next_result.value != null && next_result.value[1] != null && next_result.value[1] != undefined) {
                            setDone(true);
                        }
                    }
                    let [visited, current] = next_result.value as [Map<number, string>, GraphNode];
                    setVisited(visited);
                    setCurrent(current as MatrixGraphNode2D);
                }, 50);
                setSearchInterval(searchInterval)
            }}>Start</span> : <span style={{color: "red"}} onClick={() => {
                clearInterval(searchInterval as NodeJS.Timeout);
                setSearchInterval(null);
                let nextCurrent = new MatrixGraphNode2D(arr, [0, 0]);
                setCurrent(nextCurrent);
                setVisited(null);
                setDone(false);
            }}>Reset</span>
        )}
    </div>;
}