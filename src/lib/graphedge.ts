import { GraphNode } from './graphnode'

export interface GraphEdge {
    from(): GraphNode
    to(): GraphNode
    get?(): any
}